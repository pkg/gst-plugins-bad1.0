From: Nicolas Dufresne <nicolas.dufresne@collabora.com>
Date: Wed, 9 Aug 2023 12:49:19 -0400
Subject: h265parser: Fix possible overflow using max_sub_layers_minus1
Origin: https://gitlab.freedesktop.org/gstreamer/gstreamer/-/commit/fddda166222a067d0e511950a0a8cfb9f5a521b7
Bug-Debian: https://bugs.debian.org/1053259
Bug-Debian-Security: https://security-tracker.debian.org/tracker/CVE-2023-40476

This fixes a possible overflow that can be triggered by an invalid value of
max_sub_layers_minus1 being set in the bitstream. The bitstream uses 3 bits,
but the allowed range is 0 to 6 only.

Fixes ZDI-CAN-21768, CVE-2023-40476

Fixes https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/2895

Part-of: <https://gitlab.freedesktop.org/gstreamer/gstreamer/-/merge_requests/5366>
---
 .../gst-plugins-bad/gst-libs/gst/codecparsers/gsth265parser.c   | 2 ++
 1 file changed, 2 insertions(+)

--- a/gst-libs/gst/codecparsers/gsth265parser.c
+++ b/gst-libs/gst/codecparsers/gsth265parser.c
@@ -1845,6 +1845,7 @@ gst_h265_parse_vps (GstH265NalUnit * nal
 
   READ_UINT8 (&nr, vps->max_layers_minus1, 6);
   READ_UINT8 (&nr, vps->max_sub_layers_minus1, 3);
+  CHECK_ALLOWED (vps->max_sub_layers_minus1, 0, 6);
   READ_UINT8 (&nr, vps->temporal_id_nesting_flag, 1);
 
   /* skip reserved_0xffff_16bits */
@@ -2015,6 +2016,7 @@ gst_h265_parse_sps (GstH265Parser * pars
   READ_UINT8 (&nr, sps->vps_id, 4);
 
   READ_UINT8 (&nr, sps->max_sub_layers_minus1, 3);
+  CHECK_ALLOWED (sps->max_sub_layers_minus1, 0, 6);
   READ_UINT8 (&nr, sps->temporal_id_nesting_flag, 1);
 
   if (!gst_h265_parse_profile_tier_level (&sps->profile_tier_level, &nr,
